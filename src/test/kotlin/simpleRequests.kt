import io.restassured.RestAssured.given
import io.restassured.http.ContentType
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.Matchers.lessThan
import org.junit.jupiter.api.Test


internal class HelloWorldRestAssured {

    @Test
    fun makeSure_That_GoogleIsUp() {

        given()
            .`when`()
            .get("http://www.google.com") //initiating a GET call
            .then()
            .statusCode(200)
            .time(lessThan(250L)) //checking response time. L just represent long. It is in millisecond by default.

    }

    @Test
    fun test_ResponseHeaderData_ShouldBeCorrect() {

        given()
            .`when`()
            .get("http://ergast.com/api/f1/2017/circuits.json")
            .then()
            .assertThat()
            .statusCode(200)
            .and()
            .contentType(ContentType.JSON)
            .and()
            .header("Content-Length", equalTo("4551"))
    }

    @Test
    fun test_APIWithOAuth2Authentication_ShouldBeGivenAccess() {

        given()
            .auth()
            .oauth2("YOUR_AUTHENTICATION_TOKEN_GOES_HERE") //authorization with accessToken
            .`when`()
            .get("http://path.to/oath2/secured/api")
            .then()
            .assertThat()
            .statusCode(200)
            .log().all() //logging the response in the test results
    }

    @Test
    fun test_PostRequestToMakeABooking() {

        val requestUrl = "https://restful-booker.herokuapp.com/booking"
        val requestBody = """{
    "firstname" : "Jim",
    "lastname" : "Brown",
    "totalprice" : 111,
    "depositpaid" : true,
    "bookingdates" : {
        "checkin" : "2018-01-01",
        "checkout" : "2019-01-01"
    },
    "additionalneeds" : "Breakfast"
}"""
       given()
            .contentType("application/json")
            .`when`()
            .body(requestBody) //passing a json request body
            .post(requestUrl) //initiating a post request to create a booking
            .then()
            .log().all()
            .assertThat()
            .statusCode(200)
            .body("booking.firstname", equalTo("Jim"))
            .body("booking.depositpaid", equalTo(true))
    }

    @Test
    fun test_deleteUser() {

        given()
            .`when`()
            .delete("https://reqres.in/api/users/2") //deleting a user with id 2
            .then()
            .statusCode(204)
            .log().all()
    }
}